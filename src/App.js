import "./App.css";
import HomePage from "./pages";
import Signin from "./pages/Signin";
import Signup from "./pages/Signup";
import Services from "./pages/Services";
import About from "./pages/About";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/signin" element={<Signin />} />
        <Route path="/signup" element={<Signup />} />
        <Route path="/services" element={<Services />} />
        <Route path="/about" element={<About />} />

      </Routes>
    </Router>
  );
}

export default App;
