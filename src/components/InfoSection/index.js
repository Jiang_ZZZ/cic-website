import React from "react";
import { Button } from "../ButtonElements";
import {
  InfoSectionContainer,
  InfoRows,
  Column1,
  Column2,
  TextWrapper,
  TopLine,
  Heading,
  Subtitle,
  BtnWrap,
  ImgWrap,
  Img,
} from "./InfoSectionElements";

const InfoSection = ({
  id,
  to,
  lightBg,
  subtitleColor,
  headingColor,
  imgStart,
  topLine,
  headLine,
  description,
  buttonLabel,
  img,
  buttonDark,
  buttonPrimary,
  bigButton,
  bigfont,
}) => {
  return (
    <InfoSectionContainer lightBg={lightBg} id={id}>
      <InfoRows imgStart={imgStart}>
        <Column1>
          <TextWrapper>
            <TopLine>{topLine}</TopLine>
            <Heading headingColor={headingColor}>{headLine}</Heading>
            <Subtitle subtitleColor={subtitleColor}>{description}</Subtitle>
            <BtnWrap>
              <Button
                to={to}
                primary={buttonPrimary ? 1 : 0}
                dark={buttonDark ? 1 : 0}
                big={bigButton ? 1 : 0}
                bigfont={bigfont ? 1 : 0}
              >
                {buttonLabel}
              </Button>
            </BtnWrap>
          </TextWrapper>
        </Column1>
        <Column2>
          <ImgWrap>
            <Img src={img} />
          </ImgWrap>
        </Column2>
      </InfoRows>
    </InfoSectionContainer>
  );
};

export default InfoSection;
