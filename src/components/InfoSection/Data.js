// const svg1 = require("/images/svg-1.svg").default;
// const svg2 = require("/images/svg-2.svg").default;

export const infoObjectOne = {
  id: "services",
  to: "/services",
  lightBg: true,
  headingColor: false,
  subtitleColor: true,
  imgStart: false,
  topLine: "premium services",
  headLine: "Services to suit your needs",
  description:
    "Our services give you low barrier to entry into the Australian property market with easy liquidation. You can choose from our range of services to best suit your needs",
  buttonLabel: "Start Now",
  img: "images/svg-1.svg",
  buttonDark: false,
  buttonPrimary: false,
  bigButton: true,
  bigfont: false,
};

export const infoObjectTwo = {
  id: "about",
  to: "/about",
  lightBg: false,
  headingColor: false,
  subtitleColor: true,
  imgStart: true,
  topLine: "About us",
  headLine: "We are an Australian Owned Company",
  description:
    "CoInvestCo is a fractional investment platform with the mission to make property accessible for all Australians.",
  buttonLabel: "Learn More",
  img: "images/svg-2.svg",
  buttonDark: true,
  buttonPrimary: true,
  bigButton: true,
  bigfont: false,
};
