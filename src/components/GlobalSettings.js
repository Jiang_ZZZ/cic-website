const colorTheme = {
  primary: "#FC8182",
  buttonColor: {
    light: "#fff",
    primary: "#FC8182",
    dark: "#41414C",
  },
  text: { dark: "#4C415B", light: "#f7f8fa" },
};

export const color = {
  NavBar: "#000",
  MobileNav: "#0d0d0d",
  NavBarLink: colorTheme.text.light,
  NavBarBtn: colorTheme.primary,
  Logo: colorTheme.text.light,
  UnderLine: colorTheme.primary,
  BtnText: {
    dark: colorTheme.text.dark,
    light: colorTheme.text.light,
  },
  BtnBg: {
    primary: colorTheme.primary,
    sec: colorTheme.buttonColor.dark,
  },
  BtnHover: { primary: colorTheme.buttonColor.dark, sec: colorTheme.primary },
  Container: {
    Dark: "#F5F5F5",
    Light: "#FFFFFF",
  },
  Text: {
    primary: colorTheme.primary,
    Dark: colorTheme.text.dark,
    Light: colorTheme.text.light,
  },
  TextHover: {
    primary: colorTheme.text.light,
    sec: colorTheme.text.dark,
  },
};

export const navBarHeight = "80px";

export const navItems = ["How it works", "Services", "About", "Contact"];
