import React, { useState } from "react";
import {
  HeroContainer,
  HeroWrapper,
  HeroH1,
  HeroP,
  HeroBtnWrapper,
  ArrowFoward,
  ArrowRight,
} from "./HeroSectionElements";
import { Button } from "../ButtonElements";
const HeroSection = () => {
  const [hover, setHover] = useState(false);

  const onHover = () => {
    setHover(!hover);
  };
  return (
    <HeroContainer>
      <HeroWrapper>
        <HeroH1>Property Investment Made Easy</HeroH1>
        <HeroP>
        Register your interest by signing up with us today and start investing
        </HeroP>
        <HeroBtnWrapper>
          <Button
            to="signup"
            onMouseEnter={onHover}
            onMouseLeave={onHover}
            primary="true"
            dark="true"
          >
            Sign up {hover ? <ArrowFoward /> : <ArrowRight />}
          </Button>
        </HeroBtnWrapper>
      </HeroWrapper>
    </HeroContainer>
  );
};

export default HeroSection;
