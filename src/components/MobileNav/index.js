import React from "react";
import {
  MobileNavContainer,
  MobileMenu,
  MobileNavLink,
  MobileNavRoute,
  MobileNavBtnWrap,
} from "./MobileNavElements";
import { navItems } from "../GlobalSettings";
const MobileNav = ({ toggle, isOpen }) => {
  const mobileNavItems = navItems.map((item, index) => {
    return (
      <MobileNavLink
        to={item.replace(/\s/g, "").toLowerCase()}
        key={index}
        onClick={toggle}
        smooth={true}
        duration={500}
        spy={true}
        offset={-80}
      >
        {item}
      </MobileNavLink>
    );
  });
  return (
    <MobileNavContainer onClick={toggle} isOpen={isOpen}>
      <MobileMenu>{mobileNavItems}</MobileMenu>
      <MobileNavBtnWrap>
        <MobileNavRoute to="/signin">Sign In</MobileNavRoute>
      </MobileNavBtnWrap>
    </MobileNavContainer>
  );
};

export default MobileNav;
