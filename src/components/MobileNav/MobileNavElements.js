import styled from "styled-components";
import { Link as LinkScroll } from "react-scroll";
import { Link as LinkRouter } from "react-router-dom";
import { color,navItems } from "../GlobalSettings";

const numOfNavItems = navItems.length;
export const MobileNavContainer = styled.aside`
  display: none;
  @media screen and (max-width: 768px) {
    width: 100%;
    height: 100%;
    position: fixed;
    background: ${color.MobileNav};
    z-index: 999;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    transition: 0.3s ease-in-out;
    opacity: ${({ isOpen }) => (isOpen ? "100%" : "0")};
    top: ${({ isOpen }) => (isOpen ? "0" : "-100%")};
  }
`;

export const MobileMenu = styled.ul`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: repeat(${numOfNavItems+1}, 80px);

  text-align: center;
  @media screen and (max-width: 480px) {
    grid-template-rows: repeat(${numOfNavItems+1}, 60px);
  }
`;

export const MobileNavLink = styled(LinkScroll)`
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 1.5rem;
  text-decoration: none;
  list-style: none;
  transition: 0.2 ease-in-out;
  text-decoration: none;
  color: ${color.Text.Light};
  cursor: pointer;

  &:hover {
    color: ${color.TextHover.primary};
    transition: 0.2s ease-in-out;
  }
`;

export const MobileNavBtnWrap = styled.div`
  display: flex;
  justify-content: center;
`;

export const MobileNavRoute = styled(LinkRouter)`
  border-radius: 10px;
  background: ${color.BtnBg.primary};
  white-space: nowrap;
  padding: 16px 64px;
  color: #010606;
  font-size: 16px;
  outline: none;
  border: none;
  text-decoration: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  &:hover {
    transition: all 0.2s ease-in-out;
    background: ${color.BtnHover.primary};
    color: ${color.TextHover.primary};
  }
`;
