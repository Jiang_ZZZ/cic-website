import React from "react";
import { BurgerContainer } from "./BurgerElements";

const BurgerIcon = ({isOpen, toggle}) => {
  return (
    <BurgerContainer open={isOpen} onClick={toggle}>
      <div />
      <div />
      <div />
    </BurgerContainer>
  );
};

export default BurgerIcon;
