import styled from "styled-components";
import { color, navBarHeight } from "../GlobalSettings";
import { Link as LinkRouter } from "react-router-dom";
import { Link as LinkScroll } from "react-scroll";

export const NavBarContainer = styled.nav`
  background: ${color.NavBar};
  height: ${navBarHeight};
  margin-top: ${"-" + navBarHeight};
  display: flex;
  justify-content: center;
  font-size: 1rem;
  position: sticky;
  top: 0;
  z-index: 1000;
  @media screen and (max-width: 960px) {
    transition: 0.8s all ease;
  }
`;

export const NavBarWrapper = styled.div`
  max-width: 1100px;
  width: 100%;
  padding: 0 24px;
  display: flex;
  justify-content: space-between;
`;

export const NavLogo = styled(LinkRouter)`
  color: ${color.Logo};
  cursor: pointer;
  font-size: 1.5rem;
  font-weight: bold;
  margin-left: 24px;
  text-decoration: none;
  display: flex;
  align-items: center;
`;

export const BurgerWrapper = styled.div`
  display: none;

  @media screen and (max-width: 768px) {
    display: flex;
    align-items: center;
    cursor: pointer;
    position: absolute;
    top: 50%;
    right: 0;
    transform: translate(-100%, -50%);    
  }
`;
export const NavMenu = styled.ul`
  margin: 0 40px;
  text-align: center;
  display: flex;
  align-items: center;
  list-style: none;
  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const NavItem = styled.li`
  height: 80px;
`;

export const NavLinks = styled(LinkScroll)`
  color: ${color.NavBarLink};
  padding: 0 0.59rem;
  height: 100%;
  cursor: pointer;
  display: flex;
  align-items: center;
  text-decoration: none;
  /* white-space: nowrap; */

  &.active {
    border-bottom: 3px solid ${color.UnderLine};
  }
`;

export const NavBtn = styled.nav`
  display: flex;
  align-items: center;
  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const NavBtnLink = styled(LinkRouter)`
  border-radius: 10px;
  background: ${color.NavBarBtn};
  white-space: nowrap;
  padding: 10px 22px;
  color: ${color.BtnText.dark};
  font-size: 16px;
  outline: none;
  border: none;
  cursor: pointer;
  text-decoration: none;
  transition: all 0.2s ease-in-out;
  &:hover {
    transition: all 0.2s ease-in-out;
    background: ${color.BtnHover.primary};
    color: ${color.TextHover.primary};
  }
`;
