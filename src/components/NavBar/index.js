import React from "react";
import {
  NavBarContainer,
  NavBarWrapper,
  NavLogo,
  BurgerWrapper,
  NavMenu,
  NavItem,
  NavLinks,
  NavBtn,
  NavBtnLink,
} from "./NavBarElements";
import { navItems } from "../GlobalSettings";
import BurgerIcon from "../BurgerIcon";
import { animateScroll as scroll } from "react-scroll";

const NavBar = ({ toggle, isOpen }) => {
  const navBarItems = navItems.map((item, index) => {
    return (
      <NavItem key={index}>
        <NavLinks
          to={item.replace(/\s/g, "").toLowerCase()}
          smooth={true}
          duration={500}
          spy={true}
          offset={-80}
        >
          {item}
        </NavLinks>
      </NavItem>
    );
  });
  const toggleHome = () => {
    scroll.scrollToTop();
  };
  return (
    <NavBarContainer >
      <NavBarWrapper>
        <NavLogo to="/" onClick={toggleHome}>CoInvestCo</NavLogo>
        <BurgerWrapper onClick={toggle}>
          <BurgerIcon isOpen={isOpen} />
        </BurgerWrapper>
        <NavMenu>{navBarItems}</NavMenu>
        <NavBtn>
          <NavBtnLink to="/signin">Sign in</NavBtnLink>
        </NavBtn>
      </NavBarWrapper>
    </NavBarContainer>
  );
};

export default NavBar;
