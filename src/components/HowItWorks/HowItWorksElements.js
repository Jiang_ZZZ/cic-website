import styled from "styled-components";
import { color } from "../GlobalSettings";

export const HowItWorksContainer = styled.div`
  max-height: auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: ${color.Container.Dark};
  padding: 30px 0;
`;

export const Title = styled.h1`
  font-size: 2.5rem;
  color: ${color.Text.primary};
  margin-bottom: 64px;
  @media screen and (max-width: 480px) {
    font-size: 2rem;
  }
`;

export const CardWrapper = styled.div`
  max-width: 1100px;
  margin: 0 auto;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-content: stretch;
  padding: 0 50px;
`;

export const Card = styled.div`
  background: transparent;
  display: flex;
  flex-direction: column;
  align-items: center;
  max-height: 340px;
  max-width: 300px;
  width: 100%;
  margin: 10px 5px;
  &:hover {
    cursor: pointer;
  }
`;

export const NumberContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  width: 30px;
  height: 30px;
  padding: 10px;
  border: 3px solid ${color.Text.primary};
`;
export const NumberWrapper = styled.div`
  color: ${color.Text.primary};
`;

export const CardIcon = styled.img`
  height: 140px;
  width: 140px;
  margin-bottom: 10px;
`;

export const CardH2 = styled.h2`
  font-size: 1rem;
  margin-bottom: 10px;
  color:${color.Text.Dark};
`;

export const CardP = styled.p`
  font-size: 1rem;
  color:${color.Text.Dark};
  text-align: center;
`;
