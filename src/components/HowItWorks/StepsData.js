export const steps = {
  1: {
    iconName: "svg-3.svg",
    title: "Register your interest",
    desc: "Register your interest by signing up. Once approved, you'll have an account.",
  },

  2: {
    iconName: "svg-4.svg",
    title: "Invest in properties",
    desc: "Choose the property and plan that best suit your needs and start investing.",
  },
  3: {
    iconName: "svg-5.svg",
    title: "Earn rental income",
    desc: "We will help manage your property while you can earn rental income per share you invest",
  },
  4: {
    iconName: "svg-6.svg",
    title: "Sell your share",
    desc: "Sell your share of the property to earn any capital gains",
  }
};
