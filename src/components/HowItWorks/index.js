import React from "react";
import {
  HowItWorksContainer,
  Title,
  CardWrapper,
  Card,
  NumberContainer,
  NumberWrapper,
  CardIcon,
  CardH2,
  CardP,
} from "./HowItWorksElements";
import { steps } from "./StepsData";
const HowItWork = () => {
  const cardItems = () => {
    const items = [];
    for (let index = 0; index < Object.keys(steps).length; index++) {
      const step = index + 1;
      items.push(
        <Card key={index}>
          <NumberContainer>
            <NumberWrapper>{step}</NumberWrapper>
          </NumberContainer>
          <CardIcon
            src={`images/${steps[step].iconName}`}
          />
          <CardH2>{steps[step].title}</CardH2>
          <CardP>{steps[step].desc}</CardP>
        </Card>
      );
    }
    return <CardWrapper>{items}</CardWrapper>;
  };

  return (
    <HowItWorksContainer id="howitworks">
      <Title>How it works</Title>
      {cardItems()}
    </HowItWorksContainer>
  );
};

export default HowItWork;
