import styled from "styled-components";
import { Link as LinkRouter } from "react-router-dom";
import { color } from "./GlobalSettings";
export const Button = styled(LinkRouter)`
  background: ${({ primary }) =>
    primary ? color.BtnBg.primary : color.BtnBg.sec};
  padding: ${({ big }) => (big ? "14px 48px" : "12px 30px")};
  color: ${({ dark }) => (dark ? color.BtnText.dark : color.BtnText.light)};
  font-size: ${(props) => (props.$bigfont ? "20px" : "16px")};
  border-radius: 10px;
  outline: none;
  border: none;
  cursor: pointer;
  text-decoration: none;
  white-space: nowrap;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: all 0.2s ease-in-out;

  &:hover {
    transition: all 0.2s ease-in-out;
    background: ${({ primary }) => (primary ? color.BtnHover.primary : color.BtnHover.sec)};
    color: ${({ primary }) => (primary ? color.TextHover.primary : color.TextHover.sec)};
  }
`;
