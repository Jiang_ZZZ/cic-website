import styled from "styled-components";
import { navBarHeight, color } from "../GlobalSettings";
import { Link as LinkRouter } from "react-router-dom";
export const NavBarContainer = styled.nav`
  background: ${color.NavBar};
  height: ${navBarHeight};
  display: flex;
  justify-content: center;
  font-size: 1rem;
  position: sticky;
  top: 0;
  z-index: 1000;
  @media screen and (max-width: 960px) {
    transition: 0.8s all ease;
  }
`;

export const NavBarWrapper = styled.div`
  max-width: 1100px;
  width: 100%;
  padding: 0 24px;
  display: flex;
  justify-content: space-between;
`;

export const NavLogo = styled(LinkRouter)`
  color: ${color.Logo};
  cursor: pointer;
  font-size: 1.5rem;
  font-weight: bold;
  margin-left: 24px;
  text-decoration: none;
  display: flex;
  align-items: center;
`;
