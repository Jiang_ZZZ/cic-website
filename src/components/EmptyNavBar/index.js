import React from "react";
import { NavBarContainer, NavBarWrapper, NavLogo } from "./EmptyNavBarElements";
const EmptyNavBar = () => {
  return (
    <>
      <NavBarContainer>
        <NavBarWrapper>
          <NavLogo to="/">CoInvestCo</NavLogo>
        </NavBarWrapper>
      </NavBarContainer>
    </>
  );
};

export default EmptyNavBar;
