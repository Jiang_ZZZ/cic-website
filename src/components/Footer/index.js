import React from "react";
import { FooterContainer, FooterWrapper, FooterLogo, LinksWrapper, FooterLinkTitle, FooterLink, WebsiteRight } from "./FooterElements";
const Footer = () => {
  return (
    <FooterContainer>
      <FooterWrapper>
        <FooterLogo to="/">CoInvestCo</FooterLogo>
        <LinksWrapper>
          <FooterLinkTitle>About us</FooterLinkTitle>
          <FooterLink to="/">How it works</FooterLink>
          <FooterLink to="/">Investors</FooterLink>
          <FooterLink to="/">Terms of service</FooterLink>
          <FooterLink to="/">About us</FooterLink>
        </LinksWrapper>
      </FooterWrapper>
      <WebsiteRight>
        {" "}
        CoInvestCo © {new Date().getFullYear()} All rights reserved.
      </WebsiteRight>
    </FooterContainer>
  );
};

export default Footer;
