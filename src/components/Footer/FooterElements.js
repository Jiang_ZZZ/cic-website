import styled from "styled-components";
import { color } from "../GlobalSettings";
import { Link as LinkRouter } from "react-router-dom";

export const FooterContainer = styled.footer`
  background: ${color.Text.Dark};
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 20px;
`;

export const FooterWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  max-width: 1100px;
  width: 100%;
  align-items: flex-start;
`;

export const FooterLogo = styled(LinkRouter)`
  color: ${color.Logo};
  justify-self: start;
  text-decoration: none;
  font-weight: bold;
  font-size: 1.5rem;
  display: flex;
  align-items: center;
  margin-left: 24px;
  cursor: pointer;
`;
export const LinksWrapper = styled.div`
  justify-self: end;
  display: flex;
  flex-direction: column;
  margin-top:5px;
`;
export const FooterLinkTitle = styled.h1`
  font-size: 14px;
  margin-bottom: 16px;
  color: ${color.Logo};
`;

export const FooterLink = styled(LinkRouter)`
  color: ${color.Text.Light};
  text-decoration: none;
  margin-bottom: 0.5rem;
  font-size: 14px;

  &:hover {
    color: ${color.Text.primary};
    transition: 0.3 ease-in-out;
  }
`;

export const WebsiteRight = styled.small`
  color: ${color.Logo};
  margin-bottom: 16px;
`;
