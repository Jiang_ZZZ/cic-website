import React from "react";
import { IoLocationSharp } from "react-icons/io5";
import { MdEmail } from "react-icons/md";
import {
  ContactContainer,
  ContactH1,
  ContactWrapper,
  ContactForm,
  ContactInput,
  ContactMsg,
  ContactButton,
  DirectContact,
  ContactList,
  ContactListItem,
  ContactIcon,
  ContactSpan,
} from "./ContactElements";

const Contact = () => {
  return (
    <ContactContainer id={"contact"}>
      <ContactH1>Contact us</ContactH1>
      <ContactWrapper>
        <ContactForm>
          <ContactInput placeholder={"Name"} />
          <ContactInput placeholder={"Email"} />
          <ContactMsg placeholder={"Message"} />
          <ContactButton>Send</ContactButton>
        </ContactForm>
        <DirectContact>
          <ContactList>
            <ContactListItem>
                <ContactIcon>
                  <IoLocationSharp />
                </ContactIcon>
              <ContactSpan>Sydney, NSW</ContactSpan>
            </ContactListItem>
            <ContactListItem>
                <ContactIcon>
                  <MdEmail />
                </ContactIcon>
              <ContactSpan>coinvestcoau@gmail.com</ContactSpan>
            </ContactListItem>
          </ContactList>
        </DirectContact>
      </ContactWrapper>
    </ContactContainer>
  );
};

export default Contact;
