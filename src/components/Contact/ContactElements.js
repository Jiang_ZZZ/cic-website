import styled from "styled-components";
import { color } from "../GlobalSettings";
export const ContactContainer = styled.section`
  background: ${color.Container.Light};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 50px;
  height: 800px;
  @media screen and (max-width: 768px) {
    padding: 100px 0;
  }
`;

export const ContactH1 = styled.h1`
  color: ${color.Text.Dark};
  font-size: 2.5rem;
  margin-bottom: 64px;
`;

export const ContactWrapper = styled.div`
  max-width: 1100px;
  width: 100%;
  display: flex;
  justify-content: space-around;
  align-items: flex-start;
  padding: 0 24px;
  @media screen and (max-width: 768px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`;

export const ContactForm = styled.form`
  background-color: transparent;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 40%;
  > * {
    /* include input textarea and button */
    width: 100%;
    border: 1px solid ${color.Text.Dark};
    border-radius: 5px;
    padding: 5px 5px;
  }
  > *:focus {
    outline: none;
  }
  @media screen and (max-width: 768px) {
    width: 80%;
    margin-bottom: 40px;
  }
`;

export const ContactInput = styled.input`
  margin-bottom: 10px;

  height: 40px;
`;

export const ContactMsg = styled.textarea`
  margin-bottom: 10px;
  height: 160px;
`;

export const ContactButton = styled.button`
  background: ${color.BtnBg.primary};
  color: ${color.BtnText.dark};
  border: none;

  &:hover {
    background: ${color.BtnHover.primary};
    color: ${color.TextHover.primary};
  }
`;

export const DirectContact = styled.div`
  @media screen and (max-width: 768px) {
    width: 80%;
  }
`;

export const ContactList = styled.ul`
  display: flex;
  flex-direction: column;
`;

export const ContactListItem = styled.li`
  list-style-type: none;
  display: flex;
  align-items: center;
  margin-bottom: 10px;
`;

export const ContactIcon = styled.div`
  color: ${color.Text.Dark};
  font-size: 1.8rem;
  cursor: pointer;
  margin-right: 20px;
  display: flex;
  align-items: center;
`;

export const ContactSpan = styled.span`
  color: ${color.Text.Dark};
`;
