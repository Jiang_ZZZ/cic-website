import React, { useState } from "react";
import NavBar from "../components/NavBar";
import MobileNav from "../components/MobileNav";
import InfoSection from "../components/InfoSection";
import { infoObjectOne, infoObjectTwo } from "../components/InfoSection/Data";
import HowItWork from "../components/HowItWorks";
import Footer from "../components/Footer";
import Contact from "../components/Contact";
import HeroSection from "../components/HeroSection";
const HomePage = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };
  return (
    <>
      <NavBar toggle={toggle} isOpen={isOpen}/>
      <MobileNav toggle={toggle} isOpen={isOpen}/>
      <HeroSection />
      <HowItWork />
      <InfoSection {...infoObjectOne} />
      <InfoSection {...infoObjectTwo} />
      <Contact />
      <Footer />
    </>
  );
};

export default HomePage;
